public class Dolphin{
	
	//defining the fields of my animal
	private double length;
	private int weight;
	private int swimSpeed;	
	
	//constructor for Dolphin object
	public Dolphin(double length, int weight, int swimSpeed){
		
		this.length = length;
		this.weight = weight;
		this.swimSpeed = swimSpeed;
	}
	
	//setter for setweight
	public void setweight(int weight){
		this.weight = weight;
	}
	
	//getter for all fields
	public double getlength(){
		return this.length;
	}
	
	public int getweight(){
		return this.weight;
	}
	
	public int getswimSpeed(){
		return this.swimSpeed;
	}
	
	// instance method (Actions) of my animal
	public void ballTrick(){
		System.out.println("The dolphin balances a ball at the tip of its mouth at a height of " + this.length/2);
	}
	
	public void jump(){
		System.out.println("The dolphin jumps " + this.weight / this.swimSpeed + " meters high");
	}
}