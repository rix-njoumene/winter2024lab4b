import java.util.Scanner;

public class VirtualPetApp{
	
	public static void main(String args[]){
		
		Scanner reader = new Scanner(System.in);
		
		/*Creating an array of Dolphin object
		*Initializing each Dolphin object with a field value using constructor
		*/
		Dolphin[] podOfDolphin = new Dolphin[2];
		
		for(int i = 0; i < podOfDolphin.length; i++){

			//Length
			System.out.println("Imput a length for the dolphin number " + i  + " (whole number)");
			double length = reader.nextDouble();
			
			// Swim Speed
			System.out.println("Imput a swim speed for the dolphin number " + i + " (whole number)");
			int swimSpeed = reader.nextInt();

			//Weight
			System.out.println("Imput a weight for the dolphin number " + i + " (whole number)");
			int weight = reader.nextInt();
			
			//creation of object dolphin
			podOfDolphin[i] = new Dolphin(length, weight, swimSpeed);
		}
		
		//printing field of last dolphin object
		System.out.println("\nInformation about Dolphin " + (podOfDolphin.length -1) );
		//before changing weight
		System.out.println("length: " + podOfDolphin[podOfDolphin.length -1].getlength());
		System.out.println("weight: " + podOfDolphin[podOfDolphin.length -1].getweight());
		System.out.println("swim speed: " + podOfDolphin[podOfDolphin.length -1].getswimSpeed());
		
		//changing weight
		System.out.println("Imput a weight for the dolphin number " + (podOfDolphin.length -1) + " (whole number)");
		podOfDolphin[podOfDolphin.length -1].setweight( reader.nextInt() );
		
		System.out.println("\nNew Information about Dolphin " + (podOfDolphin.length -1) );
		//after changing weight
		System.out.println("length: " + podOfDolphin[podOfDolphin.length -1].getlength());
		System.out.println("weight: " + podOfDolphin[podOfDolphin.length -1].getweight());
		System.out.println("swim speed: " + podOfDolphin[podOfDolphin.length -1].getswimSpeed());
		
		
		
	}
}